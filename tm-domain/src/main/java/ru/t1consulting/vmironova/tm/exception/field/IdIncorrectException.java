package ru.t1consulting.vmironova.tm.exception.field;

public final class IdIncorrectException extends AbstractFieldException {

    public IdIncorrectException() {
        super("Error! Id is incorrect.");
    }

}
