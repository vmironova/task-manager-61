package ru.t1consulting.vmironova.tm.api.service.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1consulting.vmironova.tm.enumerated.CustomSort;
import ru.t1consulting.vmironova.tm.model.AbstractUserOwnedModel;

import java.util.List;

public interface IUserOwnedService<M extends AbstractUserOwnedModel> extends IService<M> {

    @NotNull
    M add(@Nullable String userId, @NotNull M model) throws Exception;

    void clear(@Nullable String userId) throws Exception;

    @Nullable
    List<M> findAll(@Nullable String userId) throws Exception;

    @Nullable
    List<M> findAll(@Nullable String userId, @Nullable CustomSort sort) throws Exception;

    @Nullable
    M findOneById(@Nullable String userId, @Nullable String id) throws Exception;

    int getSize(@Nullable String userId) throws Exception;

    void remove(@Nullable String userId, @Nullable M model) throws Exception;

    void removeById(@Nullable String userId, @Nullable String id) throws Exception;

    void update(@Nullable String userId, @Nullable M model) throws Exception;

}
